﻿using COPMDU.Domain;
using COPMDU.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Interface
{
    public interface ITicketRepository
    {
        Task<User> Authenticate(string username, string password);
        IEnumerable<Outage> GetAllTechnician(int technicianId);
        Outage Get(int id);
        Task<string> AttachEvidence(int id, byte[] fileData);
        Task<CloseStatus> Close(int ticket, int idResolution, string description, DateTime closeTime, int user);
        Task<List<TicketResponse>> GetTicketsNM();
        Task<OutageResponse> GetTicketDetail(int id);
        Task<bool> LoadNewOutage();
        Task<List<ServiceOrder>> GetServiceOrders();
        Task InsertAll(List<Outage> outages);
        Task CloseDb(int outageId, int userId);
        IEnumerable<Outage> GetAllClosed(int technicianId);
    }
}

