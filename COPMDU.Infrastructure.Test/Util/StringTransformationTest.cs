﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using COPMDU.Domain;
using COPMDU.Infrastructure.ClassAttribute;
using COPMDU.Infrastructure.Util;
using Xunit;

namespace COPMDU.Infrastructure.Test.Util
{
    public class StringTransformationTest
    {
        private const string PAGE = @"
<?xml version=""1.0"" encoding=""utf-8"" ?><xjx><cmd n=""as"" t=""divDados"" p=""innerHTML""><![CDATA[<table width=""90%"" border=""0""><tr><td><a href=""#"" onclick=""javascript:xajax_aceitaCaso(10800);""><img src=""/media/img/aceita_caso.gif"" border=""0"" alt=""Aceitar Caso""></a></td><td><a href=""#"" onclick=""javascript:xajax_TrocarFila(10800);""><img src=""/media/img/troca_2.gif"" border=""0"" alt=""Trocar Fila""></a></td><td><a href=""#"" onclick=""javascript:xajax_MostraEvento();""><img src=""/media/img/previsao.gif"" border=""0"" alt=""Registro do Previsão""></a></td><td><a href=""#"" onclick=""javascript:xajax_MostraFechamento();""><img src=""/media/img/fechamento.gif"" border=""0"" alt=""Fechamento""></a></td><td><a href=""#"" onclick=""javascript:xajax_MostraLOG();""><img src=""/media/img/log_1.gif"" border=""0"" alt=""Log""></a></td><td><a href=""#"" onclick=""javascript:xajax_alteraOutage();""><img src=""/media/img/alterar_outage.gif"" border=""0"" alt=""Alterar outage""></a></td><td><a href=""#"" onclick=""javascript:xajax_MostraArqs();""><img src=""/media/img/upload_imagens.gif"" border=""0"" alt=""Upload de Imagens""></a></td><td>&nbsp;</td><td><a href=""#"" onclick=""javascript:xajax_mostraEvidencias();""><img src=""/media/img/evidencias.png"" border=""0"" alt=""Registro de evidências"" title=""Registro de evidências""></a></td><td width=""90%"">&nbsp;</td><td align=""right""><a href=""javascript:volta();""><img src=""/media/img/back.gif"" border=""0"" /></a></td></tr><tr><td colspan=""11""><div id=""divComp""></div></td></tr><tr><td colspan=""11""><div id=""divTicket""></div></td></tr><tr><td colspan=""11""><div id=""divInf""></div></td></tr><tr><td colspan=""11""><div id=""divTimeLine""></div></td></tr><tr><td colspan=""11""><div id=""divMsg""></div></td></tr></table>]]></cmd><cmd n=""as"" t=""divTicket"" p=""innerHTML""><![CDATA[<table width=""100%"" border=""0"" cellspacing=""1"" cellpadding=""3"" ><tr class=""titulo_ticket""><td colspan=""2"">::. Sobre o ticket .::</td></tr><td class=""ticket_data""><b>Ticket n&ordm;:</b></td><td class=""ticket_value"">9075014</td></tr><tr><td class=""ticket_data""><b>Categoria:</b></td><td class=""ticket_value"">Caso <em>(0 subcaso(s))</em></td></tr><tr><td class=""ticket_data""><b>Tipo:</b></td><td class=""ticket_value"">Outage</td></tr><tr><td class=""ticket_data""><b>Servi&ccedil;os Afetados:</b></td><td class=""ticket_value""> NET VIRTUA + NET FONE + Pay TV - Digital + NOW</td></tr><tr><td class=""ticket_data""><b>Cidade</b></td><td class=""ticket_value"">Porto Alegre</td></tr><tr><td class=""ticket_data""><b>Data:</b></td><td class=""ticket_value"">02/04/2019 11:23</td></tr><tr><td class=""ticket_data""><b>Fila:</b></td><td class=""ticket_value"">Fechados </td></tr><tr><td class=""ticket_data""><b>T&iacute;tulo:</b></td><td class=""ticket_value"">PROBLEMA HFC: COAXIAL </td></tr><tr><td class=""ticket_data""><b>Descri&ccedil;&atilde;o:</b></td><td class=""ticket_value"">OUTAGE TESTE ROBÔ SERENA # CONTRATO 72012867-4 # NOTIFICAÇÃO 849311 #</td></tr><tr bgcolor=""#6ADCA2""><td class=""ticket_data""><b>Status:</b></td><td class=""ticket_value"">Fechado</td></tr></table>]]></cmd><cmd n=""as"" t=""divInf"" p=""innerHTML""><![CDATA[<table width=""100%"" cellpadding=""3"" border=""0""><tr><td colspan=""2"" class=""titulo_ticket"">::. Dados do Outage .::</td></tr><tr><td class=""ticket_data""><b>Aberto por:</b></td><td class=""ticket_value"">Elissandra Nascimento[SUL]</td></tr><tr><td class=""ticket_data""><b>Data Inicio:</b></td><td class=""ticket_value"">02/04/2019 11:19</td></tr><tr><td class=""ticket_data""><b>Data Final:</b></td><td class=""ticket_value"">02/04/2019 11:23</td></tr><tr><td class=""ticket_data""><b>Abrang&ecirc;ncia:</b></td><td class=""ticket_value""><tr class=""localcima""><td>Distrito</td><td>Node</td><td>C&eacute;lula</td></tr><tr><td class=""ticket_value"">Cidade Baixa</td><td class=""ticket_value""><span id=""lstNodesAfetados"" name=""lstNodesAfetados"">CBXAF</span></td><td class=""ticket_value""></td></tr><tr><td class=""ticket_value"" colspan=""3""><font color=""red""><b>Local:</b> R JOSE DO PATROCINIO 462 (C&Oacute;D MDU: 610463490)</font></td></tr></td></tr><tr><td class=""ticket_data""><b>Sintoma:</b></td><td class=""ticket_value"">MDU Informativo</td></tr><tr><td class=""ticket_data""><b>Natureza:</b></td><td class=""ticket_value"">Preventiva</td></tr><tr style=""height: 40px;""><td class=""ticket_data""><b>Manobra n&ordm;:</b></td><td class=""ticket_value""><em>Sem manobra relacionada.</em></td></tr><tr><td valign=""middle"" class=""ticket_data""><b>Na URA?</b></td><td class=""ticket_value"">   <table border=""0"" width=""550px""><tr>      <td class=""ticket_value"" width=""7%"">N&Atilde;O</td></tr></table></tr><tr><td valign=""middle"" class=""ticket_data""><b>Na Central?</b></td><td class=""ticket_value"">   <table border=""0"" width=""550px""><tr>      <td class=""ticket_value"" width=""7%"">N&Atilde;O</td></tr></table><tr><tr><td valign=""middle"" class=""ticket_data""><b>No ATLAS?</b></td><td class=""ticket_value""> N&Atilde;O</td></tr><tr><td valign=""middle"" class=""ticket_data""><b>Informa&ccedil;&otilde;es adicionais<br/>na URA</b></td>      <td class=""ticket_value"">Nenhuma (apenas o sintoma do outage)</td></tr><tr><td valign=""middle"" class=""ticket_data""><b>&Aacute;rea de risco?</b></td><td class=""ticket_value"">   <table border=""0"" width=""550px""><tr>      <td class=""ticket_value"" width=""7%%"">N&Atilde;O</td></tr></table></tr><tr><td></td></tr><tr><td colspan=""2"" class=""titulo_ticket"">::. Acionamento de t&eacute;cnicos .::</td></tr><tr><td align=""left"" colspan=""2""><table style=""background: none repeat scroll 0% 0% rgb(249, 249, 249); padding: 0px;"" border=""0"" width=""100%"">
	<tbody>
		<tr>
			<td style=""padding: 0px;"">
				<fieldset>
					<div id=""formDiv"" class=""formDiv""></div>
					<div id=""grid"" align=""center""></div>
					<div id=""div_grid"" name=""div_grid"">
						<table class=""adminlist"" name=""grd_nodes"" id=""grd_nodes"" border=""1"">
							<tbody>
								<tr>
									<th class=""title"" width=""10%"">Notifica&ccedil;&atilde;o Atlas</th>
									<th class=""title"" width=""30%"">T&eacute;cnico</th>
									<th class=""title"" style=""text-align: left;"" width=""13%"">Acion.</th>
									<th style=""text-align: left;"" class=""title"" width=""13%"">Start</th>
									<th style=""text-align: left;"" class=""title"" width=""13%"">Stop</th>
									<th style=""text-align: left;"" class=""title"" width=""13%"">Sa&iacute;da</th>
									<th style=""text-align: center;"" class=""title"" width=""8%"">A&ccedil;&otilde;es</th>
								</tr>
<tr><td colspan=""7"" align=""center""><span class=""campotexto"">Sem dados disponiveis</span></td></tr>							</tbody>
						</table>
						<font color=""blue"" size=""-2""><em>&nbsp;&nbsp;Clique no nome do t&eacute;cnico para editar seus dados</em></font>
					</div>
				</fieldset>
			</td>
		</tr>
	</tbody>
</table>
</td></tr></table>]]></cmd><cmd n=""as"" t=""divMsg"" p=""innerHTML""><![CDATA[<BR><table width=""100%"" cellpadding=""3"" cellspacing=""0"" border=""0"" bordercolor=""#000000""><tr class=""titulo_ticket""><td colspan=""2"">::. Registros .::</td></tr><tr><td colspan=""2"" height=""5""></td></tr><tr style=""border-width:2px;border-style:solid;border-color:#000""><td colspan=""2"" class=""msgHeader"">SERENA (CÉLULA IE) em 02/04/2019 as 11:28</td></tr><tr><td colspan=""2"" class=""msgTicketFechado"">Ticket FECHADO. Sumário: TICKET FECHADO PELO ROBO</td></tr></table>]]></cmd></xjx>
";


        [Fact]
        public static void ConvertResponseTest()
        {
            const string testResponse = "OUTAGE  TESTE ROBÔ SERENA #CONTRATO 71688247-0# #NOTIFICAÇÃO 843795#";
            var obj = StringTransformation.ConvertResponse<ResponseTest>(testResponse);
            Assert.True(obj.Notificacao == "843795", "Dado simples da conversão para string não confere");
            Assert.True(obj.Contrato == 716882470, "Dado convertido para inteiro não bate");
        }

        [Fact]
        public static void ConvertResponseListTest()
        {
            const string testResponse = @"
                    name=Miranda Antonia 
                    <td>1</td><td>421</td><td>3</td><td>4</td>
                    | #CONTRATO 71688247-0# #NOTIFICAÇÃO 843795#|| #CONTRATO 28193412-2# #NOTIFICAÇÃO 291023#|";
            var obj = StringTransformation.ConvertResponse<ResponseParenteTest>(testResponse);

            Assert.True(obj.Name == "Miranda", "Dado simples não confere");
            Assert.Null(obj.NotManaged);

            Assert.True(obj.CityIds[0] == 1, "Dado de inteiro de lista");
            Assert.True(obj.CityIds[1] == 421, "Dado de inteiro de lista");
            Assert.True(obj.CityIds[2] == 3, "Dado de inteiro de lista");
            Assert.True(obj.CityIds[3] == 4, "Dado de inteiro de lista");
        }

        [Fact]
        public static void ConvertResponseTicketNmTest()
        {
            var obj = StringTransformation.ConvertResponse<OutageResponse>(PAGE);
            Assert.True(obj.Id == 9075014);
            Assert.True(obj.Category == "Caso");
            Assert.True(obj.SubCategoryCount == 0);
            Assert.True(obj.Type == "Outage");
            Assert.True(obj.AffectedServices == "NET VIRTUA + NET FONE + Pay TV - Digital + NOW");
            Assert.True(obj.City == "Porto Alegre");
            Assert.True(obj.Date == new DateTime(2019, 4, 2, 11, 23, 0));
            Assert.True(obj.Queue == "Fechados");
            Assert.True(obj.Title == "PROBLEMA HFC: COAXIAL");
            Assert.True(obj.Description == "OUTAGE TESTE ROBÔ SERENA # CONTRATO 72012867-4 # NOTIFICAÇÃO 849311 #");
            Assert.True(obj.Status == "Fechado");
            Assert.True(obj.OpenedBy == "Elissandra Nascimento[SUL]");
            Assert.True(obj.StartDate == new DateTime(2019, 4, 2, 11, 19, 0));
            Assert.True(obj.District == "Cidade Baixa");
            Assert.True(obj.Symptom == "MDU Informativo");
            Assert.True(obj.Nature == "Preventiva");
            Assert.True(obj.Contract == 720128674);
            Assert.True(obj.Notification == 849311);
        }


        private class ResponseParenteTest
        {
            [PageResultProperty(@"name=(\w*)")]
            public string Name { get; set; }

            [PageResultListProperty(@"<td>(.+?)</td>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
            public List<int> CityIds { get; set; }

            public string NotManaged { get; } = null;
        }

        private class ResponseTest
        {
            [PageResultProperty(@"#CONTRATO ([0-9]*)-([0-9])#", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
            public int Contrato { get; set; }

            [PageResultProperty(@"#NOTIFICAÇÃO ([0-9]*)#")]
            public string Notificacao { get; set; }
        }
    }
}
